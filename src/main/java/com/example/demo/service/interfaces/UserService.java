package com.example.demo.service.interfaces;

import com.example.demo.entity.dto.Request.UserRequestDTO;
import com.example.demo.entity.response_object.ResponseObject;

public interface UserService {
    ResponseObject register(UserRequestDTO userRequestDTO);
    ResponseObject reSendOTP(String mail);
    ResponseObject active(UserRequestDTO userRequestDTO);

    ResponseObject forgotPassword(String email);
}
