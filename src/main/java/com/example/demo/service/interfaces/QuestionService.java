package com.example.demo.service.interfaces;

import com.example.demo.entity.model.Question;

public interface QuestionService {
    Question returnAQuestion();
}
