package com.example.demo.service.impl;

import com.example.demo.entity.model.Question;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.interfaces.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    UserRepository userRepository;

    public Question returnAQuestion(){
        return new Question(1, "Q1", "question a", "Q1.jpg", false, true);
    }


}
