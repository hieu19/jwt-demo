package com.example.demo.service.impl;

import com.example.demo.entity.dto.Request.UserRequestDTO;
import com.example.demo.entity.model.User;
import com.example.demo.entity.response_object.ResponseObject;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.interfaces.UserService;
import com.example.demo.util.Helper;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final JavaMailSender mailSender;
    private final UserRepository userRepository;
    private final ModelMapper mapper;
    private final PasswordEncoder bcryptEncoder;
    private final Helper helper;

    public UserServiceImpl(UserRepository userRepository, ModelMapper mapper, PasswordEncoder bcryptEncoder, Helper helper, JavaMailSender mailSender) {
        this.userRepository = userRepository;
        this.mapper = mapper;
        this.bcryptEncoder = bcryptEncoder;
        this.helper = helper;
        this.mailSender = mailSender;
    }


    //
    public List<UserRequestDTO> getRequestList() {
        List<User> list = this.userRepository.findAll();
        List<UserRequestDTO> list2 = new ArrayList<>();
        for (User u : list) {
            list2.add(mapper.map(u, UserRequestDTO.class));
        }
        return list2;
    }

    public void sendSimpleEmail(String toEmail, String text, String subject) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(toEmail);
        message.setText(text);
        message.setSubject(subject);
        mailSender.send(message);
    }

    public ResponseObject register(UserRequestDTO user) {
        UserRequestDTO userRequestDTO = helper.getUser(user.getEmail(), getRequestList());
        if (userRequestDTO != null) {
            return new ResponseObject("400", "This email exists", null);
        } else {
            String otp = helper.generateOTP();
            user.setOtp(otp);
            user.setActive(false);
            log.info("Password: " + user.getPassword());
            user.setPassword(bcryptEncoder.encode(user.getPassword()));
            User user1 = mapper.map(user, User.class);
            this.userRepository.save(user1);
            log.info("Created account with username: " + user.getEmail() + ", encoded password: " + user.getPassword());
            return new ResponseObject("200", "User " + user.getEmail() + " register successfully. Go to your mail and active account", null);
        }
    }

    public ResponseObject reSendOTP(String email) {
        UserRequestDTO userRequestDTO = helper.getUser(email, getRequestList());
        if (userRequestDTO == null) {
            return new ResponseObject("400", "this email not exists", null);
        } else {
            String new_otp = helper.generateOTP();
            userRequestDTO.setOtp(new_otp);
            User user1 = mapper.map(userRequestDTO, User.class);
            this.userRepository.save(user1);
            sendSimpleEmail(email, new_otp, "This is your new OTP");
            return new ResponseObject("200", "Re-send OTP successfully", null);
        }
    }

    public ResponseObject active(UserRequestDTO userDTO) {
        try {
            User user = this.userRepository.findByEmail(userDTO.getEmail());
            UserRequestDTO userRequestDTO = mapper.map(user, UserRequestDTO.class);
            if (userDTO.getOtp().equals(userRequestDTO.getOtp())) {
                userRequestDTO.setActive(true);
                User user1 = mapper.map(userRequestDTO, User.class);
                this.userRepository.save(user1);
                log.info("Activated account with username: " + user.getEmail());
                return new ResponseObject("200", "Active account successfully", null);
            } else {
                log.info("Activated account with username: " + user.getEmail() + " fail. Invalid OTP");
                return new ResponseObject("400", "Wrong OTP, please check your email", null);
            }
        } catch (Exception e) {
            log.info("Activated account fail. User not found");
            return new ResponseObject("400", "User not exist!", null);
        }
    }

    @Override
    public ResponseObject forgotPassword(String email) {
            User user = this.userRepository.findByEmail(email);
            if (user != null) {
                String new_otp = helper.generateOTP();
                user.setOtp(new_otp);
                this.userRepository.save(user);
                sendSimpleEmail(email, "Click here to reset your password <a href=" +"localhost:8080/user/reset-password-step2/" + new_otp + ">Reset your password now!</a>", "Someone just request to reset your password, if you do this, please follow these steps");
                log.info("Reset OTP of account with username: " + user.getEmail() + " new OTP: " + new_otp);
                return new ResponseObject("200", "Forgot password request accepted, please go to your email and reset your password", null);
            } else {
                return new ResponseObject("400", "User not found", null);
            }
    }

    public ResponseObject resetPassword(Authentication authentication) {
        String email = authentication.getName();
        User user = userRepository.findByEmail(email);
        if (user != null) {
            user.setPassword(helper.generateOTP());
        }
        return null;
    }
}
