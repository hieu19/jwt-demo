package com.example.demo.controller;

import com.example.demo.entity.response_object.ResponseObject;
import com.example.demo.service.interfaces.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class TestController {

    @Autowired
    QuestionService questionService;

    @GetMapping("/test")
    public ResponseEntity<ResponseObject> test() {
        return ResponseEntity.ok().body(new ResponseObject("200", "OK", questionService.returnAQuestion()));
    }
    @CrossOrigin
    @GetMapping("/gpt")
    public String testGPT() {
        return "xin chao hieu19";
    }
}
