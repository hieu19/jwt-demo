package com.example.demo.controller;

import com.example.demo.entity.dto.Request.UserRequestDTO;
import com.example.demo.entity.response_object.ResponseObject;
import com.example.demo.service.interfaces.UserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public ResponseObject register(@RequestBody UserRequestDTO user) {
        return userService.register(user);
    }

    @PostMapping("/active")
    public ResponseObject active(@RequestBody UserRequestDTO user) {
        return userService.active(user);
    }

    @PostMapping("/forgot-password/{email}")
    public ResponseObject forgotPassword(@PathVariable String email) {
        return userService.forgotPassword(email);
    }

    @PostMapping("/re-send/{mail}")
    public ResponseObject reSend(@PathVariable String mail) {
        return userService.reSendOTP(mail);
    }

}
