package com.example.demo.entity.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Quiz {
    private long id;
    private String qNo;
    private Question question;
    List<Answer> answers;


}
