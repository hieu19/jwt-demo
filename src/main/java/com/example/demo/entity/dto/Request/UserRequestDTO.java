package com.example.demo.entity.dto.Request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserRequestDTO {
    private int id;
    private String full_name;
    private String email;
    private String password;
    private boolean isActive;
    private String otp;
}
