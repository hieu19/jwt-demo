package com.example.demo.config.database_config;

import com.example.demo.entity.model.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@Configuration
public class MySQLConfig {
    @Autowired
    UserRepository userRepository;

    private PasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();

    @Bean
    CommandLineRunner initDatabase(){
        return new CommandLineRunner() {
            @Override
            public void run(String... args) throws Exception {
                User admin = new User(1, "admin", "admin@gmail.com", bcryptEncoder.encode("admin"), true, null);
                User dev = new User(2, "dev", "dev@gmail.com", bcryptEncoder.encode("dev"), true, null);
                User user = new User(3, "user", "user@gmail.com", bcryptEncoder.encode("user"), true, null);
                List<User> list = List.of(admin, dev, user);
                userRepository.saveAll(list);
            }
        };
    }
}
